﻿using System.Collections;

namespace BosuGladiator
{
    public class Mob : Character
    {
        public override int HP { get; set; }

        private int atk;
        public override int ATK
        {
            get
            {
                return def;
            }
        }

        private int def;
        public override int DEF
        {
            get
            {
                return def;
            }
        }

        protected Mob(int hp, int atk, int def)
        {
            this.HP = hp;
            this.atk = atk;
            this.def = def;
        }
    }
}

