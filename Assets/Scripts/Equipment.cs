﻿using System;

namespace BosuGladiator
{
    public sealed class Equipment
    {
        public String Name;
        public String FlavorText;
        public EquipPartType EquipPartType;

        public String Color;
        public String ImageName;

        public int AtkDiff;
        public int DefDiff;
        public int HPDiff;

        public int Durabilty;
        public bool IsBroken
        {
            get
            {
                return Durabilty <= 0;
            }
        }

    }

}
