﻿using System;
using System.Collections;
using UnityEngine;

namespace BosuGladiator
{    
    public abstract class Character : MonoBehaviour
    {
        public abstract int HP { get; set; }
        public abstract int ATK { get; }
        public abstract int DEF { get; }

    }
}
