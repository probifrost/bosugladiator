﻿using System;
using System.Collections;
using UnityEngine;

namespace BosuGladiator
{

    public enum PlayerActionType
    {
        None,

        Stab,
        Cut,
        Block
    }

    public class PlayerCharacter : Character
    {
        public bool IsUnderDefence;

        public override int HP { get; set; }
        private int atk;
        public override int ATK
        {
            get
            {
                atk = 0;
                foreach (Equipment equipment in equipments)
                {
                    atk += equipment.AtkDiff;
                }
                return atk;
            }
        }
        private int def;
        public override int DEF
        {
            get
            {
                def = 0;

                if (IsUnderDefence)
                    def += 50;

                foreach (Equipment equipment in equipments)
                {
                    def += equipment.DefDiff;
                }                
                return def;
            }
        }

        public PlayerActionType currentActionType;

        private void Start()
        {

        }

        public void SetAction(String actionTypeName)
        {
            SetAction((PlayerActionType)Enum.Parse(typeof(PlayerActionType), actionTypeName));
        }
        public void SetAction(PlayerActionType actionType)
        {
            if (currentActionType != PlayerActionType.None)
                Debug.LogError("CurrentActionType is not none");

            currentActionType = actionType;
        }


        //Equipment part
        private Equipment[] equipments;

        protected PlayerCharacter()
        {
            equipments = new Equipment[(int)EquipPartType.Max];
        }

        public bool HasItem(EquipPartType partType)
        {
            return GetItem(partType) != null;
        }
        public Equipment GetItem(EquipPartType partType)
        {
            return equipments[(int)partType];
        }
        public void SetItem(EquipPartType partType, Equipment equip)
        {
            if (HasItem(partType))
                Debug.LogError($"{partType} 타입 파츠 이미 있음!: {GetItem(partType).Name}");
            equipments[(int)partType] = equip;
        }
    }
}
