﻿namespace BosuGladiator
{
    public enum EquipPartType
    {
        Unknown = 0,

        Head,
        Body,
        Weapon,
        Shield,
        Robe,
        Shoe,
        Skirt,
        Accessory,

        Max,
    }
}
