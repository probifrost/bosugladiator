﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BosuGladiator
{

    public class TextManager : MonoBehaviour
    {
        private GameObject player = GameObject.Find("Player");
        private Text HPtext = GameObject.Find("HPText").GetComponent<Text>();
        private Text AtkText = GameObject.Find("AttText").GetComponent<Text>();
        private Text DefText = GameObject.Find("DefText").GetComponent<Text>();

        public void UpdateHP()
        {
            HPtext.text = "HP : " + player.GetComponent<PlayerCharacter>().HP;
        }
        public void UpdateAtt()
        {
            AtkText.text = "ATK : " + player.GetComponent<PlayerCharacter>().ATK;
        }
        public void UpdateDef()
        {
            DefText.text = "DEF : " + player.GetComponent<PlayerCharacter>().DEF;
        }
    }
}
