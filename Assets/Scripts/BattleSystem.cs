﻿using System.Collections;
using UnityEngine;

namespace BosuGladiator
{
    public class BattleSystem : MonoBehaviour
    {
        public bool IsDefense;

        private PlayerCharacter Player;
        private Mob Enemy;
        private bool IsPlayer;


        // Start is called before the first frame update
        void Start()
        {
            Player = GameObject.Find("Player").GetComponent<PlayerCharacter>();
            Enemy = GameObject.Find("Enemy").GetComponent<Mob>();
            IsPlayer = true;
            CallNextCombatAction();
        }

        // Update is called once per frame
        void Update()
        {

        }

        int GetDamage(int damage, int def)
        {
            if (damage - def >= 0)
            {
                return damage - def;
            }
            else return 0;
        }
        bool IsBattleOver() //true = Battle Finish, false -> Keep Battle
        {
            if (Player.HP <= 0 || Enemy.HP <= 0)
            {
                return true;
            }
            return false;
        }

        public void CallNextCombatAction()
        {
            // 전투 끝 체크
            //if (IsBattleOver())
            //{
            //    Debug.Log("전투 끝");
            //    return;
            //}

            // 다음 캐릭 COmbatAction 호출
            if (IsPlayer)
            {
                StartCoroutine(PlayerCombatActionCoroutine().GetEnumerator());
            }
            else
            {
                StartCoroutine(EnemyCombatActionCoroutine().GetEnumerator());
            }
        }

        public IEnumerable PlayerCombatActionCoroutine()
        {
            Player.IsUnderDefence = false;
            Player.currentActionType = PlayerActionType.None;

            while (Player.currentActionType == PlayerActionType.None)
            {
                yield return new WaitForEndOfFrame();
            }


            Debug.Log(Player.currentActionType);

            // 타입에 맞게 처리
            if (Player.currentActionType == PlayerActionType.Stab)
            {
                int skillCoeff = 200;
                PlayerAttack(skillCoeff);
            }
            if (Player.currentActionType == PlayerActionType.Cut)
            {
                int skillCoeff = 100;
                PlayerAttack(skillCoeff);
            }
            if (Player.currentActionType == PlayerActionType.Block)
            {
                PlayerDefend();
            }

            // 데미지 넣기
        }

        public IEnumerable EnemyCombatActionCoroutine()
        {
            throw new System.NotImplementedException();
        }

        //Player Logic
        public void PlayerAttack(int skillCoeff)
        {
            int damage = GetDamage(Player.ATK * skillCoeff / 100, Enemy.DEF);
            Enemy.HP -= damage;
            IsPlayer = !IsPlayer;
        }
        public void PlayerDefend()
        {
            Player.IsUnderDefence = true;
        }

        //Enemy Logic
        public void EnemyAttack()
        {
            int damage = GetDamage(Enemy.ATK, Player.DEF);
            Player.HP -= damage;
            IsPlayer = !IsPlayer;
        }
        public void EnemyDefend()
        {

        }
    }
}

